package com.example.telusko1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Telusko1Application {

	public static void main(String[] args) {
		SpringApplication.run(Telusko1Application.class, args);
	}

}
