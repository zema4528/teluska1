package com.example.telusko1.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.telusko1.dao.AlianRepository;
import com.example.telusko1.model.Alian;

@RestController
public class AlienRestApi {

	@Autowired
	private AlianRepository alianRepository;
	
	@RequestMapping(path = "/aliens", produces = {"application/xml"}, 
			method = RequestMethod.GET)
	public List<Alian> getAllAliens(){
		return alianRepository.findAll(); 
	}
	
	@RequestMapping(path = "/aliens/{id}", produces = {"application/json"}, 
			method = RequestMethod.GET)
	public Alian getById(@PathVariable("id") Integer id) {
		return alianRepository.findById(id).get();
	}
}
