package com.example.telusko1.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.telusko1.model.Alian;

@Controller
public class HomeController {

	// first way
	@RequestMapping("home")
	public String home(HttpServletRequest rec) {

		HttpSession session = rec.getSession();
		String name = rec.getParameter("name");
		System.out.println("Hello!" + name);
		session.setAttribute("name", name);
		return "home";
	}

	// second way usingModelAndView
	@RequestMapping("home2")
	public ModelAndView home2(@RequestParam("name") String name) {

		ModelAndView mv = new ModelAndView("home");
		mv.addObject("name", name);
	//	mv.setViewName("home");
		return mv;
	}

	// Third example if you want to send an object by request
	// like this send an Object http://localhost:8080/home3?id=1&name=Azim&lang=Java
		@RequestMapping("home3")
		public ModelAndView home3(Alian alian) {

			ModelAndView mv = new ModelAndView("home");
			mv.addObject("obj", alian);
		//	mv.setViewName("home");
			return mv;
		}
}
