package com.example.telusko1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.telusko1.dao.AlianRepository;
import com.example.telusko1.model.Alian;

@Controller
public class AlienController {
	
	@Autowired
	private AlianRepository alianRepository;
	
	@RequestMapping("/")
	public String addAlien() {
		return "mvcExample";
	}

	@RequestMapping("/addAlien")
	public String addAlien(Alian alian) {
		alianRepository.save(alian);
		return "mvcExample";
	}
	
	@RequestMapping("/getAlien")
	public ModelAndView getAlien(@RequestParam Integer id) {

		ModelAndView mv = new ModelAndView("getAlien");
		Alian alien = alianRepository.findById(id).get();
		mv.addObject("alien", alien);
		return mv;
	}

	
	
	
	
	
}