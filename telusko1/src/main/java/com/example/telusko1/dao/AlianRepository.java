package com.example.telusko1.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.telusko1.model.Alian;

@Repository
public interface AlianRepository extends JpaRepository<Alian, Integer>{

}
